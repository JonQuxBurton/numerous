﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numerous.Domain
{
    public class PuzzleGenerator : IPuzzleGenerator
    {
        public Puzzle Generate()
        {
            return new Puzzle(new int[] { 3, 4 }, new ICommand[] { new AddCommand() });
        }
    }
}
