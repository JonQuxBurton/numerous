﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numerous.Domain
{
    public sealed class Puzzle
    {
        private readonly IEnumerable<int> numbers;
        private readonly IEnumerable<ICommand> commands;

        public Puzzle(IEnumerable<int> numbers, IEnumerable<ICommand> commands)
        {
            if (numbers == null)
                throw new ArgumentNullException("numbers");

            if (commands == null)
                throw new ArgumentNullException("commands");

            this.numbers = numbers;
            this.commands = commands;
        }

        public IEnumerable<int> Numbers
        {
            get { return numbers; }
        }

        public IEnumerable<ICommand> Commands
        {
            get { return commands; }
        }
    }
}
