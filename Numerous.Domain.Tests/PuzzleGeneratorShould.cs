﻿using Ploeh.AutoFixture.Xunit2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Numerous.Domain.Tests
{
    public class PuzzleGeneratorShould
    {
        [Fact]
        public void GeneratePuzzle()
        {
            var generator = new PuzzleGenerator();

            var puzzle = generator.Generate();

            Assert.NotNull(puzzle);
            Assert.NotEmpty(puzzle.Numbers);
            Assert.True(puzzle.Numbers.Count() > 1);
            Assert.NotEmpty(puzzle.Commands);
            Assert.True(puzzle.Commands.All(c => c != null));
        }
    }
}
