﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Numerous.Controllers
{
    public class HomeController : Controller
    {
        private Domain.IPuzzleGenerator puzzleGenerator = new Domain.PuzzleGenerator();

        public ActionResult Index()
        {
            var puzzle = puzzleGenerator.Generate();

            return View(puzzle);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult SubmitSolution(SubmittedSolution submittedSolution)
        {
            return Json(new { success = true });
        }
    }

    public class SubmittedSolution
    {
        public string Solution { get; set; }
    }
}