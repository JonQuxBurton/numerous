﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Numerous.Startup))]
namespace Numerous
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
